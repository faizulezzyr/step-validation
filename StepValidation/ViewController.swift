//
//  ViewController.swift
//  StepValidation
//
//  Created by Innovadeaus on 12/4/20.
//  Copyright © 2020 Innovadeaus. All rights reserved.
//

import UIKit
import CoreLocation
import HealthKit

class ViewController: UIViewController {
    @IBOutlet weak var lat: UILabel!
    @IBOutlet weak var lng: UILabel!
    @IBOutlet weak var dCovered: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var startStopB: UIButton!
    
    var zeroTime = TimeInterval()
    var timer : Timer = Timer()
    var locationManager: CLLocationManager!
    var healthStore = HKHealthStore()
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    var currentLocation : CLLocation!
    var distanceTraveled = 0.0
   // let healthManager:HealthKitManager = HealthKitManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       authorizeHealthKit()
        initLocationManager()
        // Do any additional setup after loading the view.
    }
    func initLocationManager(){
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
           startMonitoringLocation()
    }
    private func authorizeHealthKit() {
      
      HealthKitSetupAssistant.authorizeHealthKit { (authorized, error) in
        
        guard authorized else {
          
          let baseMessage = "HealthKit Authorization Failed"
          
          if let error = error {
            print("\(baseMessage). Reason: \(error.localizedDescription)")
          } else {
            print(baseMessage)
          }
          
          return
        }
        
        print("HealthKit Successfully Authorized.")
      }
      
    }
    
    @IBAction func startStop(_ sender: Any) {
        if  startStopB.titleLabel?.text == "Start" {
            startStopB.setTitle("Stop", for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector:  #selector(updateTime), userInfo: nil, repeats: true)
            zeroTime = NSDate.timeIntervalSinceReferenceDate
            distanceTraveled = 0.0
            startLocation = currentLocation
            lastLocation = nil
        }else{
            startStopB.setTitle("Start", for: .normal)
            timer.invalidate()
            lastLocation = currentLocation
            let lastDistance = startLocation.distance(from: currentLocation)
            distanceTraveled += lastDistance * 0.000621371
            let DistanceInMet = distanceTraveled * 1609.34
            self.dCovered.text = "\(DistanceInMet)m"
            locationManager.stopUpdatingLocation()
        }
    }
    
    @objc func updateTime() {
        let currentTime = NSDate.timeIntervalSinceReferenceDate
        var timePassed: TimeInterval = currentTime - zeroTime
        let minutes = UInt8(timePassed / 60.0)
        timePassed -= (TimeInterval(minutes) * 60)
        let seconds = UInt8(timePassed)
        timePassed -= TimeInterval(seconds)
        let millisecsX10 = UInt8(timePassed * 100)
        
        let strMinutes = String(format: "%02d", minutes)
        let strSeconds = String(format: "%02d", seconds)
        let strMSX10 = String(format: "%02d", millisecsX10)
        
        time.text = "\(strMinutes):\(strSeconds):\(strMSX10)"

    }
    

}

extension ViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lastLocation = locations.last {
            currentLocation = lastLocation
            self.lat.text = String(lastLocation.coordinate.latitude)
            self.lng.text = String(lastLocation.coordinate.longitude)
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            break
        case .restricted:
            break
        case .denied:
            stopMonitoringLocation()
            break
        default:
            //startMonitoringLocation()
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Errors: " + error.localizedDescription)
    }
    func startMonitoringLocation() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.activityType = CLActivityType.automotiveNavigation
            locationManager.distanceFilter = 1
            locationManager.headingFilter = 1
            locationManager.requestWhenInUseAuthorization()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopMonitoringLocation() {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
    }
}

